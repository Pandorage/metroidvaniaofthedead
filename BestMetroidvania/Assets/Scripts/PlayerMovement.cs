﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public FloatVariable movementSpeed;
    public BoolVariable canMove;
    private Rigidbody2D _rb;
    private Vector2 _movement;
    private Animator _anim;
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _anim = GetComponent<Animator>();
    }
    
    void Update()
    {
        _movement.x = Input.GetAxisRaw("Horizontal");
        _movement.y = Input.GetAxisRaw("Vertical");
    }

    private void FixedUpdate()
    {
        if (canMove.value)
        {
            _anim.SetFloat("Horizontal", _movement.x);
            _anim.SetFloat("Vertical", _movement.y);
            _anim.SetFloat("Speed", _movement.sqrMagnitude);
            float coef = 1;
            if (_movement.x != 0 && _movement.y != 0)
            {
                coef = 0.75f;
            }
            _rb.velocity = (_movement * movementSpeed.value * coef);
        }
    }

    public void EndIntro()
    {
        canMove.value = true;
    }
}
